using System;
using System.Collections;
using System.Collections.Generic;
using Units;
using UnityEngine;

public enum TurnPhase
{
    Player1, //this should be by default our player
    Player2
}

public enum ActionPhase
{
    Attack,
    Movement
}

public class TurnManager : MonoBehaviour
{
    public TurnPhase TurnPhase { get; private set; }
    public ActionPhase ActionPhase { get; private set; }
    
    public static TurnManager Instance;

    public event Action<TurnPhase> OnTurnPhaseChanged;
    public event Action<ActionPhase> OnActionPhaseChanged; 

    private void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(Instance);
        }
        else
        {
            Instance = this;
        }

        Unit.StaticOnMovementFinished += OnUnitMovementFinished;
        Unit.StaticOnAttackFinished += OnUnitAttackFinished;
    }

    private void Start()
    {
        Initialize();
    }

    private void OnDestroy()
    {
        Unit.StaticOnMovementFinished -= OnUnitMovementFinished;
        Unit.StaticOnAttackFinished -= OnUnitAttackFinished;
    }

    private void Initialize()
    {
        SetTurnPhase(TurnPhase.Player1);
        SetActionPhase(ActionPhase.Movement);
    }

    private void SwitchPlayersTurn()
    {
        SetTurnPhase(TurnPhase == TurnPhase.Player1 ? TurnPhase.Player2 : TurnPhase.Player1);
    }
    
    private void OnUnitMovementFinished(Unit unit)
    {
        SetActionPhase(ActionPhase.Attack);
        Debug.Log("Unit movement finished");
    }

    private void OnUnitAttackFinished(Unit unit)
    {
        Debug.Log("On unit attack finished called");
        SetActionPhase(ActionPhase.Movement);
        OnUnitActionsFinished();
    }
    
    private void OnUnitActionsFinished()
    {
        SetActionPhase(ActionPhase.Movement);
        SwitchPlayersTurn();
    }
    
    private void SetActionPhase(ActionPhase phase)
    {
        ActionPhase = phase;
        OnActionPhaseChanged?.Invoke(ActionPhase);
    }

    private void SetTurnPhase(TurnPhase phase)
    {
        TurnPhase = phase;
        OnTurnPhaseChanged?.Invoke(TurnPhase);
    }
    
}
