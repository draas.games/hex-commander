
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{

    private Camera _camera;

    [SerializeField] private float speed = 20f;
    
    private void Start()
    {
        _camera = Camera.main;
    }

#if UNITY_EDITOR
    private void Update()
    {
        if (Input.GetKey(KeyCode.W))
        {
            MoveCamera(Vector3.up, speed * Time.deltaTime);
        }
        if (Input.GetKey(KeyCode.S))
        {
            MoveCamera(Vector3.down, speed * Time.deltaTime);
        }
        if (Input.GetKey(KeyCode.A))
        {
            MoveCamera(Vector3.left, speed * Time.deltaTime);
        }
        if (Input.GetKey(KeyCode.D))
        {
            MoveCamera(Vector3.right, speed * Time.deltaTime);
        }

        if (Input.GetKey(KeyCode.Space)) //вверх
        {
            MoveCamera(Vector3.forward, speed * Time.deltaTime);
        }
        if (Input.GetKey(KeyCode.C)) //вниз
        {
            MoveCamera(Vector3.back, speed * Time.deltaTime);
        }
    }
#endif
    
    
    private void MoveCamera(Vector3 direction, float speed)
    {
        Debug.Log("Moving camera");
        _camera.transform.Translate(direction * speed);
    }
}

