using System;
using System.Collections;
using System.Collections.Generic;
using Hexes;
using Selection;
using Units;
using UnityEngine;

public class UnitMovementManager : MonoBehaviour
{
    [SerializeField] private HexGrid grid;

    [SerializeField] private MovementSystem movementSystem;

    [SerializeField] private Unit selectedUnit;
    private BaseHex previousSelectedHex;

    private void Awake()
    {
        SelectionManager.OnUnitSelected += HandleUnitSelected;
        SelectionManager.OnTerrainSelected += HandleTerrainSelected;
    }

    private void OnDestroy()
    {
        SelectionManager.OnUnitSelected -= HandleUnitSelected;
        SelectionManager.OnTerrainSelected -= HandleTerrainSelected;
    }

    private void HandleUnitSelected(GameObject unit)
    {
        if(TurnManager.Instance.TurnPhase != TurnPhase.Player1 || unit.GetComponent<Unit>().IsEnemy) return;

        if (TurnManager.Instance.ActionPhase == ActionPhase.Attack)
        {
            return;
        }
        
        Unit unitRef = unit.GetComponent<Unit>();
        
        if(CheckIfSameUnitSelected(unitRef)) return;

        PrepareUnitForMovement(unitRef);
    }

    private void HandleTerrainSelected(GameObject hex)
    {
        if (selectedUnit == null || TurnManager.Instance.TurnPhase != TurnPhase.Player1) return;

        BaseHex selectedHex = hex.GetComponent<BaseHex>();

        if (HandleHexOutOfRange(selectedHex.HexCoords) || HandleSelectedHexIsUnitHex(selectedHex.HexCoords))
            return;

        HandleTargetHexSelected(selectedHex);
    }
    
    private void HandleTargetHexSelected(BaseHex selectedHex)
    {
        if (previousSelectedHex == null || previousSelectedHex != selectedHex)
        {
            previousSelectedHex = selectedHex;
            movementSystem.ShowPath(selectedHex.HexCoords, grid);
        }
        else
        {
            selectedUnit.SetCurrentHex(selectedHex);
            movementSystem.MoveUnit(selectedUnit, grid);
            selectedUnit.OnMovementFinished += SelectedUnitOnOnMovementFinished;
            ClearOldSelection();
        }
    }

    private void SelectedUnitOnOnMovementFinished(Unit unit)
    {
        unit.OnMovementFinished -= SelectedUnitOnOnMovementFinished;
    }

    private bool HandleSelectedHexIsUnitHex(Vector3Int hexPosition)
    {
        if (hexPosition == grid.GetClosestHex(selectedUnit.transform.position))
        {
            selectedUnit.Deselect();
            ClearOldSelection();
            return true;
        }

        return false;
    }

    private bool HandleHexOutOfRange(Vector3Int hexPosition)
    {
        if (movementSystem.IsHexInRange(hexPosition) == false)
        {
            return true;
        }

        return false;
    }

    private void PrepareUnitForMovement(Unit unit)
    {
        if(selectedUnit != null) ClearOldSelection();

        selectedUnit = unit;
        selectedUnit.Select();
        
        movementSystem.ShowRange(selectedUnit, grid);
    }

    private bool CheckIfSameUnitSelected(Unit unit)
    {
        if (this.selectedUnit == unit)
        {
            ClearOldSelection();
            return true;
        }

        return false;
    }

    private void ClearOldSelection()
    {
        previousSelectedHex = null;
        selectedUnit.Deselect();
        movementSystem.HideRange(grid);
        selectedUnit = null;
    }
}
