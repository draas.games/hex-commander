using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Hexes;
using Units;
using UnityEngine;

public class MovementSystem : MonoBehaviour
{
    private BFSResult movementRange = new BFSResult();
    private List<Vector3Int> currentPath = new List<Vector3Int>();

    public void HideRange(HexGrid grid)
    {
        foreach (var position in movementRange.GetRangePositions())
        {
            grid.GetTileAt(position).ToggleHighlight(false);
        }

        movementRange = new BFSResult();
    }

    public void ShowRange(Unit selectedUnit, HexGrid grid)
    {
        CalculateRange(selectedUnit, grid);

        Vector3Int unitPosition = grid.GetClosestHex(selectedUnit.transform.position);

        foreach (var position in movementRange.GetRangePositions())
        {
            if(unitPosition == position) continue;
            
            grid.GetTileAt(position).ToggleHighlight(true);
        }
    }

    private void CalculateRange(Unit selectedUnit, HexGrid grid)
    {
        movementRange = GraphSearch.BFSGetRange(
            grid, 
            grid.GetClosestHex(selectedUnit.transform.position), 
            selectedUnit.GetMovementPoints());
    }

    public void ShowPath(Vector3Int selectedHexPosition, HexGrid grid)
    {
        if (movementRange.GetRangePositions().Contains(selectedHexPosition))
        {
            Debug.Log("Reached here");
            
            foreach (var position in currentPath)
            {
                grid.GetTileAt(position).ResetHighlight();
            }
            
            currentPath = movementRange.GetPathTo(selectedHexPosition);
            
            foreach (var position in currentPath)
            {
                grid.GetTileAt(position).HighlightPath();
            }
        }
    }

    public void MoveUnit(Unit selectedUnit, HexGrid grid)
    {
        if (TurnManager.Instance.ActionPhase != ActionPhase.Movement)
        {
            Debug.Log("Current phase is not movement!");
            return;
        }
        selectedUnit.MoveThroughPath(currentPath.Select(pos => grid.GetTileAt(pos).transform.position).ToList());
    }

    public bool IsHexInRange(Vector3Int hexPosition)
    {
        return movementRange.IsHexPositionInRange(hexPosition);
    }
}
