using System;
using System.Collections;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;

public class BundleLoader : MonoBehaviour
{
    private const int RetryCount = 3;

    public event Action OnDownloadFinished;
    public event Action OnDownloadFailed;

        
    public IEnumerator LoadBundleFromServer(AssetReference assetReference, Action<float> downloadProgressCallback)
    {
        bool isLoaded = false;
        int currentRetryCount = 0;
            
        while (!isLoaded)
        {
            AsyncOperationHandle downloadDependencies = Addressables.DownloadDependenciesAsync(assetReference);
            
            Debug.Log($"Started loading {assetReference.Asset}");
            
            yield return new WaitUntil(() =>
            {
                Debug.Log($"Downloading: {downloadDependencies.GetDownloadStatus().Percent} percentages");

                return downloadDependencies.IsDone;
            });
            switch (downloadDependencies.Status)
            {
                case AsyncOperationStatus.Succeeded:
                    Debug.Log("Download finished successfully!");
                    OnDownloadFinished?.Invoke();
                    isLoaded = true;
                    break;
                case AsyncOperationStatus.Failed:
                    
                    currentRetryCount++;
                    
                    if (currentRetryCount < RetryCount)
                    {
                        downloadDependencies = Addressables.DownloadDependenciesAsync(assetReference);
                        continue;
                    }
                    else
                    {
                        Debug.Log("Download failed!");
                        OnDownloadFailed?.Invoke();
                        break;
                    }
            }
        }
        
      
    }

}

