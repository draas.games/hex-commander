using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.ResourceManagement.ResourceLocations;
using UnityEngine.ResourceManagement.ResourceProviders;
using UnityEngine.SceneManagement;

namespace Downloadable
{
    [Serializable]
    public class AssetReferenceScene : AssetReference
    {
        public AssetReferenceScene(string guid) : base(guid) {}
 
        //-----------------------------------------------------------------------------
 
        public override bool ValidateAsset(string path) 
        {
            return path.EndsWith(".unity");
        }
    }

    public class AddressablesManager : MonoBehaviour
    {
        [SerializeField] private AssetReference sceneReference;

        private void Start()
        {
            Addressables.GetDownloadSizeAsync(sceneReference);
        }

        public void DownloadAsset(AssetReference asset, Action<float> callback)
        {
            var isLoaded = false;
            
            AsyncOperationHandle handler = Addressables.DownloadDependenciesAsync(sceneReference);
            
            handler.Completed += operation =>
            {
                if (operation.Status == AsyncOperationStatus.Succeeded)
                {
                    // Scene loaded successfully
                    // Do something with the sceneInstance if needed

                }
                else
                {
                    // Scene loading failed
                    Debug.LogError("Failed to load scene: " + operation.OperationException);
                }
            };

        }
    }
}
