using UnityEngine;

namespace Selection
{
    public interface ISelectable
    {
        public void Select();
        public void Deselect();
    }
}
