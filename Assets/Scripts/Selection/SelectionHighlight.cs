using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectionHighlight : MonoBehaviour
{
    private Dictionary<Renderer, Material[]> glowMaterialDictionary = new Dictionary<Renderer, Material[]>();
    private Dictionary<Renderer, Material[]> originalMaterialDictionary = new Dictionary<Renderer, Material[]>();
    private Dictionary<Color, Material> cachedGlowMaterials = new Dictionary<Color, Material>();

    public Material glowMaterial;

    private bool _isGlowing = false;

    private Color validSpaceColor = Color.green;
    private Color originalGlowColor;

    private void Awake()
    {
        PrepareMaterialDictionaries();
        originalGlowColor = glowMaterial.GetColor("_GlowColor");
    }

    public void ToggleGlow()
    {
        if (_isGlowing == false)
        {
            ResetGlowHighlight();
            foreach (var renderer in originalMaterialDictionary.Keys)
            {
                renderer.materials = glowMaterialDictionary[renderer];
            }
        }
        else
        {
            foreach (var renderer in originalMaterialDictionary.Keys)
            {
                renderer.materials = originalMaterialDictionary[renderer];
            }
        }

        _isGlowing = !_isGlowing;
    }

    public void ToggleGlow(bool state)
    {
        if(_isGlowing == state) return;

        _isGlowing = !state;
        ToggleGlow();
    }
    
    private void PrepareMaterialDictionaries()
    {
        foreach (var renderer in GetComponentsInChildren<Renderer>())
        {
            Material[] originalMaterials = renderer.materials;
            originalMaterialDictionary.Add(renderer, originalMaterials);
            Material[] newMaterials = new Material[renderer.materials.Length];
            for (int i = 0; i < originalMaterials.Length; i++)
            {
                Material mat = null;
                if (cachedGlowMaterials.TryGetValue(originalMaterials[i].color, out mat) == false)
                {
                    mat = new Material(glowMaterial);

                    mat.color = originalMaterials[i].color;
                    cachedGlowMaterials[mat.color] = mat;
                }

                newMaterials[i] = mat;
            }
            
            glowMaterialDictionary.Add(renderer, newMaterials);
        }
    }

    public void ResetGlowHighlight()
    {
        //if(!_isGlowing) return;

        foreach (var renderer in glowMaterialDictionary.Keys)
        {
            foreach (var item in glowMaterialDictionary[renderer])
            {
                item.SetColor("_GlowColor", originalGlowColor);
            }
        }
    }

    public void HighlightPath()
    {
        if(!_isGlowing) return;

        foreach (var renderer in glowMaterialDictionary.Keys)
        {
            foreach (var item in glowMaterialDictionary[renderer])
            {
                item.SetColor("_GlowColor", validSpaceColor);
            }
        }
    }
}
