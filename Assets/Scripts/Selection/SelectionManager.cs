using System;
using Player;
using Units;
using UnityEngine;

namespace Selection
{
    public class SelectionManager : MonoBehaviour
    {
        private Camera _camera;
        [SerializeField] private LayerMask selectionMask;

        public static event Action<GameObject> OnUnitSelected;
        public static event Action<GameObject> OnTerrainSelected; 

        private void Awake()
        {
            _camera = Camera.main;
            PlayerInput.OnPointerClick += HandleClick;
        }

        private void OnDestroy()
        {
            PlayerInput.OnPointerClick -= HandleClick;
        }

        private void HandleClick(Vector3 mousePosition)
        {
            GameObject result;

            if (FindTarget(mousePosition, out result))
            {
                // if (SelectableSelected(result))
                // {
                //     //smth happens
                // }
                if (UnitSelected(result))
                {
                    OnUnitSelected?.Invoke(result);
                }
                else
                {
                    OnTerrainSelected?.Invoke(result);
                }
            }
        }

        private bool SelectableSelected(GameObject result)
        {
            return result.GetComponent<ISelectable>() != null;
        }
        
        private bool UnitSelected(GameObject result)
        {
            return result.GetComponent<Unit>() != null;
        }

        private bool FindTarget(Vector3 mousePosition, out GameObject result)
        {
            Debug.Log("Find target called");
            RaycastHit hit;
            Ray ray = _camera.ScreenPointToRay(mousePosition);
            Debug.DrawRay(_camera.transform.position, ray.direction * 100, Color.blue, 5f);
            if (Physics.Raycast(ray, out hit, 100, selectionMask))
            {
                Debug.Log("Result found");
                result = hit.collider.gameObject;
                return true;
            }

            Debug.Log("Result not found");
            result = null;
            return false;
        }
    }
}
