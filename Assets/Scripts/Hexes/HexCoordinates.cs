using System;
using UnityEngine;

namespace Hex
{
    public class HexCoordinates : MonoBehaviour
    {
        public static float xOffset = 2;
        public static float yOffset = 1;
        public static float zOffset = 1.73f;

        [SerializeField] private Vector3Int offsetCoordinates;

        private void Awake()
        {
            offsetCoordinates = ConvertPositionToOffset(transform.position);
        }

        public void CalculatePosition()
        {
            offsetCoordinates = ConvertPositionToOffset(transform.position);
        }

        public Vector3Int GetHexCoords()
        {
            return offsetCoordinates;
        }

        public static Vector3Int ConvertPositionToOffset(Vector3 position)
        {
            int x = Mathf.CeilToInt(position.x / xOffset);
            int y = Mathf.RoundToInt(position.y / yOffset);
            int z = Mathf.RoundToInt(position.z / zOffset);
            return new Vector3Int(x, y, z);
        }
    }
}
