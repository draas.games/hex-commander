using System;
using UnityEngine;

namespace Hexes
{
    public class HexMapGenerator : MonoBehaviour
    {
        public static event Action OnMapGenerated;
        
        public int mapWidth = 10; // Number of hexes in a row
        public int mapHeight = 10; // Number of hexes in a column
        public float hexSize = 1f; // Size of each hex tile

        public float hexHorizontalSpacing = 0.1f;
        public float hexVerticalSpacing = 0.1f;

        public GameObject hexPrefab;

        private void Start()
        {
            GenerateGrid();
        }

        public void GenerateGrid()
        {
            // Calculate the width and height of the hex grid in world space
            float gridWidth = mapWidth * hexSize * hexHorizontalSpacing;
            float gridHeight = mapHeight * hexSize * hexVerticalSpacing;

            // Create a container GameObject to hold the hex tiles
            GameObject gridContainer = new GameObject("HexGrid");
            gridContainer.transform.position = new Vector3(0, 0, 0);

            // Loop through each row and column to position and instantiate the hex tiles
            for (int row = 0; row < mapHeight; row++)
            {
                for (int col = 0; col < mapWidth; col++)
                {
                    // Calculate the position of the current hex tile
                    float xPos = col * hexSize * hexHorizontalSpacing;
                    float yPos = row * hexSize * hexVerticalSpacing;

                    // Offset every other row by half a hex size for the hexagonal pattern
                    if (row % 2 == 1)
                        xPos += hexSize * hexHorizontalSpacing * 0.5f;

                    // Create a new GameObject for the hex tile
                    GameObject hexTile = Instantiate(hexPrefab, gridContainer.transform);
                    hexTile.transform.position = new Vector3(xPos, 0f, yPos);
                    hexTile.GetComponent<BaseHex>().Initialize();
                }
            }

            // Center the grid container in the scene
            //gridContainer.transform.position = new Vector3(-gridWidth / 2f, 0f, -gridHeight / 2f);
            
            Debug.Log("Hex map generated");
            OnMapGenerated?.Invoke();
        }
    }
}
