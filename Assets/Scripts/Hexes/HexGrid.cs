using System;
using System.Collections.Generic;
using Hex;
using UnityEngine;

namespace Hexes
{
    public class HexGrid : MonoBehaviour
    {
        private Dictionary<Vector3Int, BaseHex> hexTileDict = 
            new Dictionary<Vector3Int, BaseHex>();

        private Dictionary<Vector3Int, List<Vector3Int>> hexTileNeighbours =
            new Dictionary<Vector3Int, List<Vector3Int>>();
        
        private void Awake()
        {
            HexMapGenerator.OnMapGenerated += HexMapGeneratorOnOnMapGenerated;
        }

        private void OnDestroy()
        {
            HexMapGenerator.OnMapGenerated -= HexMapGeneratorOnOnMapGenerated;
        }

        private void HexMapGeneratorOnOnMapGenerated()
        {
            var hexes = FindObjectsOfType<BaseHex>();

            foreach (BaseHex hex in hexes)
            {
                hexTileDict.TryAdd(hex.HexCoords, hex);
            }
        }

        public BaseHex GetTileAt(Vector3Int hexCoordinates)
        {
            BaseHex result = null;
            hexTileDict.TryGetValue(hexCoordinates, out result);
            return result;
        }

        public List<Vector3Int> GetNeighboursFor(Vector3Int hexCoordinates)
        {
            Debug.Log($"Hex coordinates are {hexCoordinates}");
            
            if (hexTileDict.ContainsKey(hexCoordinates) == false)
            {
                Debug.Log("Coord not found");
                return new List<Vector3Int>();
            }

            if (hexTileNeighbours.ContainsKey(hexCoordinates))
            {
                Debug.Log("Key already exists");
                return hexTileNeighbours[hexCoordinates];
            }

            Debug.Log("Initializing smth new");
            
            hexTileNeighbours.Add(hexCoordinates, new List<Vector3Int>());

            foreach (var direction in Direction.GetDirectionList(hexCoordinates.z))
            {
                Debug.Log("Direction is " + direction);
                Debug.Log($"Key to search is {direction + hexCoordinates}");
                if (hexTileDict.ContainsKey(hexCoordinates + direction))
                {
                    Debug.Log("Match found!");
                    hexTileNeighbours[hexCoordinates].Add(hexCoordinates + direction);
                }
            }

            return hexTileNeighbours[hexCoordinates];
        }

        public Vector3Int GetClosestHex(Vector3 worldPosition)
        {
            worldPosition.y = 0;
            return HexCoordinates.ConvertPositionToOffset(worldPosition);
        }
    }

    public static class Direction
    {
        public static List<Vector3Int> directionOffsetOdd = new List<Vector3Int>
        {
            new Vector3Int(-1, 0, 1),
            new Vector3Int(0, 0, 1),
            new Vector3Int(1, 0, 0),
            new Vector3Int(0, 0, -1),
            new Vector3Int(-1, 0, -1),
            new Vector3Int(-1, 0, 0)
        };
        
        public static List<Vector3Int> directionOffsetEven = new List<Vector3Int>
        {
            new Vector3Int(0, 0, 1),
            new Vector3Int(1, 0, 1),
            new Vector3Int(1, 0, 0),
            new Vector3Int(1, 0, -1),
            new Vector3Int(0, 0, -1),
            new Vector3Int(-1, 0, 0)
        };

        public static List<Vector3Int> GetDirectionList(int z)
        {
            return z % 2 == 0 ? directionOffsetEven : directionOffsetOdd;
        }
    }
}
