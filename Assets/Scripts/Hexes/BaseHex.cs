using System;
using Hex;
using UnityEngine;

namespace Hexes
{
    public enum HexType
    {
        None, 
        Default,
        Forest,
        Swamp,
        Obstacle
    }
    
    [SelectionBase]
    public class BaseHex : MonoBehaviour
    {
        private HexCoordinates _hexCoordinates;
        private SelectionHighlight _highlight;

        public HexType hexType;
        
        public Vector3Int HexCoords => _hexCoordinates.GetHexCoords();
        public bool IsOccupied { get; private set; }
        
        public void Initialize()
        {
            _highlight = GetComponent<SelectionHighlight>();
            _hexCoordinates = GetComponent<HexCoordinates>();
            _hexCoordinates.CalculatePosition();
        }

        public void ToggleHighlight(bool state)
        {
            _highlight.ToggleGlow(state);
        }

        public int GetCost()
        {
            switch (hexType)
            {
                case HexType.Default:
                    return 5;
                    break;
                case HexType.Forest:
                    return 10;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public bool IsObstacle()
        {
            return this.hexType == HexType.Obstacle;
        }

        public void ResetHighlight()
        {
            _highlight.ResetGlowHighlight();
        }

        public void HighlightPath()
        {
            _highlight.HighlightPath();
        }

        public void SetOccupied(bool state)
        {
            IsOccupied = state;
        }
    }
}
