using System.Collections;
using Units;
using UnityEngine;

namespace Test
{
    public class EnemyAI : MonoBehaviour
    {

        [SerializeField] private Unit enemyUnit;
        [SerializeField] private Unit unitToAttack;
        
        private void Start()
        {
            TurnManager.Instance.OnTurnPhaseChanged += InstanceOnOnTurnPhaseChanged;
        }

        private void OnDestroy()
        {
            TurnManager.Instance.OnTurnPhaseChanged -= InstanceOnOnTurnPhaseChanged;
        }
        
        private void InstanceOnOnTurnPhaseChanged(TurnPhase phase)
        {
            Logger.LogMessage("Force end enemy turn");
            if (phase == TurnPhase.Player2) StartCoroutine(ForceEndTurn());
        }

        private IEnumerator ForceEndTurn()
        {
            yield return new WaitForSeconds(3f);
            
            enemyUnit.Attack(unitToAttack);
            Debug.Log("Attacked unit");
            //end some turn
        }
    }
}
