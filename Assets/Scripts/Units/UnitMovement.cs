using System;
using System.Collections;
using System.Collections.Generic;
using Hexes;
using UnityEngine;

namespace Units
{
    public class UnitMovement : MonoBehaviour, IMove
    {
        private Unit _unit;
        [SerializeField] private UnitData unitData;

        public BaseHex currentHex;
        //public int movementPoints = 20;

        private int currentHealth;
        
        public float movementDuration = 1f;
        public float rotationDuration = 0.3f;

        private SelectionHighlight _highlight;
        private Queue<Vector3> pathPositions = new Queue<Vector3>();

        public event Action<Unit> OnMovementFinished;

        public void Move(Vector3 endPosition)
        {
            
        }
        
        public int GetMovementPoints()
        {
            return unitData.MovementPoints;
        }

        public void MoveThroughPath(List<Vector3> currentPath)
        {
            pathPositions = new Queue<Vector3>(currentPath);
            Vector3 firstTarget = pathPositions.Dequeue();
            StartCoroutine(RotationCoroutine(firstTarget, rotationDuration, true));
        }

        private IEnumerator RotationCoroutine(Vector3 endPosition, float rotationDuration, bool firstRotation = false)
        {
            Quaternion startRotation = transform.rotation;
            endPosition.y = transform.position.y;
            Vector3 direction = endPosition - transform.position;

            Quaternion endRotation = Quaternion.LookRotation(direction, Vector3.up);

            if (Mathf.Approximately(Mathf.Abs(Quaternion.Dot(startRotation, endRotation)), 1f) == false)
            {
                float timeElapsed = 0f;
                while (timeElapsed > rotationDuration)
                {
                    timeElapsed += Time.deltaTime;
                    float lerpStep = timeElapsed / rotationDuration;
                    transform.rotation = Quaternion.Lerp(startRotation, endRotation, lerpStep);
                    yield return null;
                }

                transform.rotation = endRotation;
            }

            StartCoroutine(MovementCoroutine(endPosition));
        }

        private IEnumerator MovementCoroutine(Vector3 endPosition)
        {
            Vector3 startPosition = transform.position;
            endPosition.y = startPosition.y;
            float timeElapsed = 0f;

            while (timeElapsed < movementDuration)
            {
                timeElapsed += Time.deltaTime;
                float lerpStep = timeElapsed / movementDuration;
                transform.position = Vector3.Lerp(startPosition, endPosition, lerpStep);
                yield return null;
            }

            transform.position = endPosition;

            if (pathPositions.Count > 0)
            {
                Debug.Log("Selecting next position!");
                StartCoroutine(RotationCoroutine(pathPositions.Dequeue(), rotationDuration));
            }
            else
            {
                Debug.Log("Movement finished!");
                OnMovementFinished?.Invoke(_unit);
            }
        }
    }
}


