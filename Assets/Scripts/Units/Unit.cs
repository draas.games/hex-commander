using System;
using System.Collections;
using System.Collections.Generic;
using Hexes;
using Selection;
using UnityEngine;

namespace Units
{
    [SelectionBase]
    public class Unit : MonoBehaviour, IMove, IRotate, IAttack, ISelectable, IDamageable, IDie
    {
        /// <summary>
        /// Test purpose only
        /// </summary>
        public bool SkipAttack = true;
        
        [SerializeField] private UnitData unitData;

        public UnitType unitType = UnitType.Infantry;

        public bool IsEnemy = true;

        public BaseHex previousHex;
        public BaseHex currentHex;

        private int currentHealth;
        
        public float movementDuration = 1f;
        public float rotationDuration = 0.3f;

        private SelectionHighlight _highlight;
        private Queue<Vector3> pathPositions = new Queue<Vector3>();

        public static event Action<Unit> StaticOnMovementFinished;
        public static event Action<Unit> StaticOnAttackFinished; 

        public event Action<Unit> OnMovementFinished;
        public event Action<Unit> OnAttackFinished; 
        public event Action<Unit> OnUnitDied; 
        
        private void Awake()
        {
            _highlight = GetComponent<SelectionHighlight>();
            currentHealth = unitData.MaxHealth;
        }

        public void Move(Vector3 endPosition)
        {
            throw new NotImplementedException();
        }

        public void Rotate()
        {
            throw new NotImplementedException();
        }

        public void SetCurrentHex(BaseHex hex)
        {
            previousHex = currentHex;
            currentHex = hex;
        }

        public virtual void Attack(Unit target)
        {
            if (target == null)
            {
                Debug.LogWarning("Attack target is null!");
                return;
            }

            // Debug.Log(currentHex.HexCoords);
            // Debug.Log(target.currentHex.HexCoords);
            //
            // Debug.Log($"Current hex coords are {currentHex.HexCoords}, target unit hex coords are {target.currentHex.HexCoords}");
            //
            // if (GetDistanceToHex(currentHex, target.currentHex) < unitData.AttackDistance)
            // {
            //     Debug.Log("Distance is bigger than attack distance. Cannot attack");
            //     return;
            // }
            
            target.TakeDamage(10);
            OnAttackFinished?.Invoke(this);
            StaticOnAttackFinished?.Invoke(this);
        }
        
        public void TakeDamage(int damage)
        {
            Debug.Log($"{this} took damage. Health is {currentHealth}");
            currentHealth -= damage;
            if (currentHealth <= 0)
            {
                Die();
            }
        }

        public void Die()
        {
            OnUnitDied?.Invoke(this);
            gameObject.SetActive(false);
        }
        
        public void Select()
        {
            _highlight.ToggleGlow();
        }

        public void Deselect()
        {
            _highlight.ToggleGlow(false);
        }

        public int GetMovementPoints()
        {
            return unitData.MovementPoints;
        }

        private int GetDistanceToHex(BaseHex hex, BaseHex targetHex)
        {
            return (int)Vector3Int.Distance(hex.HexCoords, targetHex.HexCoords);
        }
        
        public void MoveThroughPath(List<Vector3> currentPath)
        {
            previousHex?.SetOccupied(false);
            currentHex.SetOccupied(true);
            
            pathPositions = new Queue<Vector3>(currentPath);
            Vector3 firstTarget = pathPositions.Dequeue();
            StartCoroutine(RotationCoroutine(firstTarget, rotationDuration, true));
        }

        private IEnumerator RotationCoroutine(Vector3 endPosition, float rotationDuration, bool firstRotation = false)
        {
            Quaternion startRotation = transform.rotation;
            endPosition.y = transform.position.y;
            Vector3 direction = endPosition - transform.position;

            Quaternion endRotation = Quaternion.LookRotation(direction, Vector3.up);

            if (Mathf.Approximately(Mathf.Abs(Quaternion.Dot(startRotation, endRotation)), 1f) == false)
            {
                float timeElapsed = 0f;
                while (timeElapsed > rotationDuration)
                {
                    timeElapsed += Time.deltaTime;
                    float lerpStep = timeElapsed / rotationDuration;
                    transform.rotation = Quaternion.Lerp(startRotation, endRotation, lerpStep);
                    yield return null;
                }

                transform.rotation = endRotation;
            }

            StartCoroutine(MovementCoroutine(endPosition));
        }

        private IEnumerator MovementCoroutine(Vector3 endPosition)
        {
            Vector3 startPosition = transform.position;
            endPosition.y = startPosition.y;
            float timeElapsed = 0f;

            while (timeElapsed < movementDuration)
            {
                timeElapsed += Time.deltaTime;
                float lerpStep = timeElapsed / movementDuration;
                transform.position = Vector3.Lerp(startPosition, endPosition, lerpStep);
                yield return null;
            }

            transform.position = endPosition;

            if (pathPositions.Count > 0)
            {
                Debug.Log("Selecting next position!");
                StartCoroutine(RotationCoroutine(pathPositions.Dequeue(), rotationDuration));
            }
            else
            {
                Debug.Log("Movement finished!");
                OnMovementFinished?.Invoke(this);
                StaticOnMovementFinished?.Invoke(this);

                if (SkipAttack) StartCoroutine(SkipAttackCoroutine());
            }
        }

        private IEnumerator SkipAttackCoroutine()
        {
            yield return new WaitForSeconds(1f);
            
            StaticOnAttackFinished?.Invoke(this);
        }
    }
}
