using UnityEngine;

namespace Units
{
    public enum UnitType
    {
        Infantry,
        Cavalry,
        Ranged
    }

    public interface IDamageable
    {
        public void TakeDamage(int damage);
    }
    
    public interface IDie
    {
        public void Die();
    }
    
    public interface IAttack
    {
        public void Attack(Unit target);
    }
    
    public interface IMove
    {
        public void Move(Vector3 endPosition);
    }
    
    public interface IRotate
    {
        public void Rotate();
    }

}
