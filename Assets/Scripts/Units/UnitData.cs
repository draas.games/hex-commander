using UnityEngine;

namespace Units
{
    [CreateAssetMenu(menuName = "Unit Data", fileName = "UnitData")]
    public class UnitData : ScriptableObject
    {
        [Header("Base stats")]
        [SerializeField] private int movementPoints = 20;
        [SerializeField] private int attackDistance = 3;
        [SerializeField] private int maxHealth = 20;

        public int MovementPoints => movementPoints;
        public int AttackDistance => attackDistance;
        public int MaxHealth => maxHealth;
    }
}
