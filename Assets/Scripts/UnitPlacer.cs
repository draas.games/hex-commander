using System;
using System.Collections;
using System.Collections.Generic;
using Hexes;
using Selection;
using Units;
using UnityEngine;

public class UnitPlacer : MonoBehaviour
{
    [SerializeField] private Vector3 offset = Vector3.up;
    //public GameObject unitPrefab;
    public int unitsToPlace = 3;
    [SerializeField] private GameObject[] units;
    private int currentUnit = 0;
    
    private void Start()
    {
        SelectionManager.OnTerrainSelected += OnTerrainSelected;
        
        //offset.y = unitPrefab. 
    }

    private void OnDestroy()
    {
        SelectionManager.OnTerrainSelected -= OnTerrainSelected;
    }

    private void OnTerrainSelected(GameObject obj)
    {
        if(currentUnit >= units.Length) return;
        
        var hex = obj.GetComponent<BaseHex>();
        
        if(hex.IsOccupied) return;

        GameObject unit = Instantiate(units[currentUnit], obj.transform.position + offset, Quaternion.identity);
        unit.GetComponent<Unit>().SetCurrentHex(hex);
        hex.SetOccupied(true);

        currentUnit++;
    }
}
