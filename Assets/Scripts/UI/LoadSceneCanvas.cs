using System;
using System.Collections;
using TMPro;
using TMPro.Examples;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace UI
{
    public class LoadSceneCanvas : MonoBehaviour
    {

        [SerializeField] private Button loadSceneButton;
        [SerializeField] private Button exitApplicationButton;
        [SerializeField] private AssetReference sceneReference;
        [SerializeField] private TextMeshProUGUI bundleSize;

        [SerializeField] private AssetReference cube;

        [SerializeField] private BundleLoader loader;

        private Action<float> _downloadCallback;

        private void Start()
        {
            loadSceneButton.onClick.AddListener(LoadScene);
            exitApplicationButton.onClick.AddListener(Exit);
            _downloadCallback = DownloadCallback;
        }

        private void DownloadCallback(float value)
        {
            bundleSize.SetText(value.ToString());
        }

        private void LoadScene()
        {
            loader.OnDownloadFinished += LoaderOnOnDownloadFinished;
            
            StartCoroutine(loader.LoadBundleFromServer(sceneReference, f => {}));
        }

        private void LoaderOnOnDownloadFinished()
        {
            loader.OnDownloadFinished -= LoaderOnOnDownloadFinished;
            Addressables.LoadSceneAsync(sceneReference);
        }

        private void Exit()
        {
            Application.Quit();
        }
    }
}
