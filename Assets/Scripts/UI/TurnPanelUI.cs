using TMPro;
using UnityEngine;

namespace UI
{
    public class TurnPanelUI : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI turnText;
        [SerializeField] private TextMeshProUGUI actionText;
        
        private string player1turn = "Ход игрока 1";
        private string player2turn = "Ход игрока 2";
        
        private string movement = "Фаза движения";
        private string attack = "Фаза атаки";
        
        private void Start()
        {
            TurnManager.Instance.OnTurnPhaseChanged += InstanceOnOnTurnPhaseChanged;
            TurnManager.Instance.OnActionPhaseChanged += InstanceOnOnActionPhaseChanged;
        }
        
        private void OnDestroy()
        {
            TurnManager.Instance.OnTurnPhaseChanged -= InstanceOnOnTurnPhaseChanged;
            TurnManager.Instance.OnActionPhaseChanged -= InstanceOnOnActionPhaseChanged;
        }
        
        private void InstanceOnOnTurnPhaseChanged(TurnPhase phase)
        {
            turnText.text = phase switch
            {
                TurnPhase.Player1 => player1turn,
                TurnPhase.Player2 => player2turn,
                _ => turnText.text
            };
        }
        
        private void InstanceOnOnActionPhaseChanged(ActionPhase phase)
        {
            actionText.text = phase switch
            {
                ActionPhase.Attack => attack,
                ActionPhase.Movement => movement,
                _ => actionText.text
            };
        }
        
        
    }
}
