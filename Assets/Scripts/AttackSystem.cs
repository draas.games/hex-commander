using System;
using Selection;
using Units;
using UnityEngine;

public class AttackSystem : MonoBehaviour
{
    private Unit _selectedUnit;
    private Unit _unitToAttack;
    
    private void Awake()
    {
        SelectionManager.OnUnitSelected += SelectionManagerOnOnUnitSelected;
        SelectionManager.OnTerrainSelected += SelectionManagerOnOnTerrainSelected;
    }

    private void OnDestroy()
    {
        SelectionManager.OnUnitSelected -= SelectionManagerOnOnUnitSelected;
        SelectionManager.OnTerrainSelected -= SelectionManagerOnOnTerrainSelected;
    }

    private void SelectionManagerOnOnTerrainSelected(GameObject obj)
    {
        if(TurnManager.Instance.ActionPhase != ActionPhase.Attack) return;

        if (!_unitToAttack) return;
        
        _unitToAttack.Deselect();
        _unitToAttack = null;
    }

    private void SelectionManagerOnOnUnitSelected(GameObject obj)
    {
        if (TurnManager.Instance.ActionPhase != ActionPhase.Attack) return;

        var unit = obj.GetComponent<Unit>();
        
        if (!unit.IsEnemy)
        {
            _selectedUnit = unit;
            _selectedUnit.Select();
            return;
        }
        
        if (IsUnitToAttackNull())
        {
            _unitToAttack = unit;
            _unitToAttack.Select();
        }
        else if (ChoseOtherEnemyUnit(unit))
        {
            _unitToAttack.Deselect();
            _unitToAttack = unit;
            _unitToAttack.Select();
        }
        else if (UnitToAttackIsSame(unit))
        {
            _selectedUnit.Attack(_unitToAttack);
            
            ClearSelection();
            // _selectedUnit.Deselect();
            // _unitToAttack.Deselect();
            // _unitToAttack = null;
        }
    }

    private void ClearSelection()
    {
        _selectedUnit.Deselect();
        _unitToAttack.Deselect();

        _selectedUnit = null;
        _unitToAttack = null;
    }
    
    private bool UnitToAttackIsSame(Unit unit)
    {
        return _unitToAttack != null && unit == _unitToAttack;
    }

    private bool ChoseOtherEnemyUnit(Unit unit)
    {
        return _unitToAttack != null && _unitToAttack != unit;
    }

    private bool IsUnitToAttackNull()
    {
        return _unitToAttack == null;
    }
}
