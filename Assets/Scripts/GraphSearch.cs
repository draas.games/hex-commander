using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Hexes;
using Units;
using UnityEngine;

public class GraphSearch : MonoBehaviour
{

    /// <summary>
    /// Deeply WIP!!! Do not use it unless you know what are you doing
    /// </summary>
    public static BFSResult BFSGetRangeForUnit(Unit unit, HexGrid grid, Vector3Int startPoint, int movementPoints)
    {
        Dictionary<Vector3Int, Vector3Int?> visitedNodes = new Dictionary<Vector3Int, Vector3Int?>();
        Dictionary<Vector3Int, int> costSoFar = new Dictionary<Vector3Int, int>();
        Queue<Vector3Int> nodeToVisitQueue = new Queue<Vector3Int>();
        
        nodeToVisitQueue.Enqueue(startPoint);
        costSoFar.Add(startPoint, 0);
        visitedNodes.Add(startPoint, null);

        while (nodeToVisitQueue.Count > 0)
        {
            Vector3Int currentNode = nodeToVisitQueue.Dequeue();

            foreach (var neighbourPosition in grid.GetNeighboursFor(currentNode))
            {
                if(grid.GetTileAt(neighbourPosition).IsObstacle() ||
                   grid.GetTileAt(neighbourPosition).IsOccupied) continue;

                var tile = grid.GetTileAt(neighbourPosition).GetComponent<BaseHex>();

                int nodeCost = ModifyNodeCostBasedOnUnitType(unit, tile);
                
                //int nodeCost = grid.GetTileAt(neighbourPosition).GetCost();
                int currentCost = costSoFar[currentNode];
                int newCost = currentCost + nodeCost;

                if (newCost <= movementPoints)
                {
                    if (!visitedNodes.ContainsKey(neighbourPosition))
                    {
                        visitedNodes[neighbourPosition] = currentNode;
                        costSoFar[neighbourPosition] = newCost;
                        nodeToVisitQueue.Enqueue(neighbourPosition);
                    }
                    else if (costSoFar[neighbourPosition] > newCost)
                    {
                        costSoFar[neighbourPosition] = newCost;
                        visitedNodes[neighbourPosition] = currentNode;
                    }
                }
            }
        }

        return new BFSResult { visitedNodesDict = visitedNodes };
    }

    private static int ModifyNodeCostBasedOnUnitType(Unit unit, BaseHex tile)
    {
        int modifiedCost = tile.GetCost();
        
        switch (unit.unitType)
        {
            case UnitType.Infantry:
                if (tile.hexType == HexType.Default)
                {
                    
                }
                break;
            case UnitType.Cavalry:

                break;
            case UnitType.Ranged:

                break;
            default:
                throw new Exception("Unit type is not supported!");
                break;
        }

        return modifiedCost;
    }


    public static BFSResult BFSGetRange(HexGrid grid, Vector3Int startPoint, int movementPoints)
    {
        Dictionary<Vector3Int, Vector3Int?> visitedNodes = new Dictionary<Vector3Int, Vector3Int?>();
        Dictionary<Vector3Int, int> costSoFar = new Dictionary<Vector3Int, int>();
        Queue<Vector3Int> nodeToVisitQueue = new Queue<Vector3Int>();
        
        nodeToVisitQueue.Enqueue(startPoint);
        costSoFar.Add(startPoint, 0);
        visitedNodes.Add(startPoint, null);

        while (nodeToVisitQueue.Count > 0)
        {
            Vector3Int currentNode = nodeToVisitQueue.Dequeue();

            foreach (var neighbourPosition in grid.GetNeighboursFor(currentNode))
            {
                if(grid.GetTileAt(neighbourPosition).IsObstacle() ||
                   grid.GetTileAt(neighbourPosition).IsOccupied) continue;

                int nodeCost = grid.GetTileAt(neighbourPosition).GetCost();
                int currentCost = costSoFar[currentNode];
                int newCost = currentCost + nodeCost;

                if (newCost <= movementPoints)
                {
                    if (!visitedNodes.ContainsKey(neighbourPosition))
                    {
                        visitedNodes[neighbourPosition] = currentNode;
                        costSoFar[neighbourPosition] = newCost;
                        nodeToVisitQueue.Enqueue(neighbourPosition);
                    }
                    else if (costSoFar[neighbourPosition] > newCost)
                    {
                        costSoFar[neighbourPosition] = newCost;
                        visitedNodes[neighbourPosition] = currentNode;
                    }
                }
            }
        }

        return new BFSResult { visitedNodesDict = visitedNodes };
    }

    public static List<Vector3Int> GeneratePathBFS(Vector3Int current, Dictionary<Vector3Int, Vector3Int?> visitedNodesDict)
    {
        List<Vector3Int> path = new List<Vector3Int>();
        path.Add(current);

        foreach (var node in visitedNodesDict)
        {
            Debug.Log($"Node key/value is {node.Key} {node.Value}");
        }

        //TODO как-то предохраняться от бесконечного цикла
        while (visitedNodesDict[current] != null)
        {
            path.Add(visitedNodesDict[current].Value); 
            current = visitedNodesDict[current].Value;
        }
        
        path.Reverse();
        return path.Skip(1).ToList();
    }
}

public struct BFSResult
{
    public Dictionary<Vector3Int, Vector3Int?> visitedNodesDict;

    public List<Vector3Int> GetPathTo(Vector3Int destination)
    {
        if (visitedNodesDict.ContainsKey(destination) == false)
        {
            return new List<Vector3Int>();
        }

        return GraphSearch.GeneratePathBFS(destination, visitedNodesDict);
    }

    public bool IsHexPositionInRange(Vector3Int position)
    {
        return visitedNodesDict.ContainsKey(position);
    }

    public IEnumerable<Vector3Int> GetRangePositions()
    {
        return visitedNodesDict.Keys;
    }
}
